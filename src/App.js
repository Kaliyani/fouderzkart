import React, { Component } from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import './App.css';
import Header from './component/Header/Header'
import HomePage from './component/Home/HomePageGrid'
import Bottom from '../src/component/Footer/Bottom'
import ProductCard from './component/Product/ProductSearch'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Founders from '../src/component/founders/Founders'

library.add(fab)

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Header/>
          <Switch>
              <Route exact path='/' component={HomePage}/>
              <Route path='/stores' component={Founders}/>
              <Route path='/product' component={ProductCard}/>
              <Route path='/ethicalqualities' component={HomePage}/>
              <Route path='/newarrivals' component={HomePage}/>
              <Route path='/event' component={HomePage}/>
          </Switch>   
          <Bottom/>
        </div>
      </Router>
    );
  }
}

export default App;

import foundersOne from './founders1.jpg'
import foundersTwo from './founders2.jpg'
import foundersThree from './founders3.jpg'
import foundersFour from './founders4.jpg'
export default{
    'founders':[
        {
            'id':1,
            'founderName': 'Chekku Ennai',
            'founderImage' : foundersOne,
            'founderImageTitle' : 'title1',
            'founderDescription' : `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since 2014`,
            'totalProduct' : [
                {
                    productId:1,
                    productName:'One'
                },
                {
                    productId:1,
                    productName:'One'
                },
                {
                    productId:1,
                    productName:'One'
                },
                {
                    productId:1,
                    productName:'One'
                },
                {
                    productId:1,
                    productName:'One'
                }
            ]
        },
        {
            'id':2,
            'founderName': 'Vidiyal Pathipagam',
            'founderImage' : foundersTwo,
            'founderImageTitle' : 'title2',
            'founderDescription' : `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since 2014`,
            totalProduct : [
                {
                    productId:1,
                    productName:'One'
                },
                {
                    productId:1,
                    productName:'One'
                },
                {
                    productId:1,
                    productName:'One'
                },
                {
                    productId:1,
                    productName:'One'
                },
                {
                    productId:1,
                    productName:'One'
                }
            ]
        },
        {
            'id':3,
            'founderName': 'A2 Milk',
            'founderImage' : foundersThree,
            'founderImageTitle' : 'title3',
            'founderDescription' : `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since 2014`,
            totalProduct : [
                {
                    productId:1,
                    productName:'One'
                },
                {
                    productId:1,
                    productName:'One'
                },
                {
                    productId:1,
                    productName:'One'
                },
                {
                    productId:1,
                    productName:'One'
                },
                {
                    productId:1,
                    productName:'One'
                }
            ]
        },
        {
            'id':4,
            'founderName': 'Nattu Sakkarai',
            'founderImage' : foundersFour,
            'founderImageTitle' : 'title4',
            'founderDescription' : `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since 2014`,
            totalProduct : [
                {
                    productId:1,
                    productName:'One'
                },
                {
                    productId:1,
                    productName:'One'
                },
                {
                    productId:1,
                    productName:'One'
                },
                {
                    productId:1,
                    productName:'One'
                },
                {
                    productId:1,
                    productName:'One'
                }
            ]
        }
    ]
}
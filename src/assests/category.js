import book from './cbook.jpg'
import household from './chousehold.jpg'
import nutrition from './cnutrition.jpg'
import snacks from './csnacks.jpg'
export default{
    'categories':[
        {
            'id':1,
            'categoryName': 'books',
            'categoryImage' : book,
            'categoryImageTitle' : 'title1'
        },
        {
            'id':2,
            'categoryName': 'snacks',
            'categoryImage' : snacks,
            'categoryImageTitle' : 'title2'
        },
        {
            'id':3,
            'categoryName': 'nutrition',
            'categoryImage' : nutrition,
            'categoryImageTitle' : 'title3'
        },
        {
            'id':4,
            'categoryName': 'household',
            'categoryImage' : household,
            'categoryImageTitle' : 'title4'
        },
        {
            'id':5,
            'categoryName': 'books',
            'categoryImage' : book,
            'categoryImageTitle' : 'title5'
        },
        {
            'id':6,
            'categoryName': 'snacks',
            'categoryImage' : snacks,
            'categoryImageTitle' : 'title6'
        },
        {
            'id':7,
            'categoryName': 'nutrition',
            'categoryImage' : nutrition,
            'categoryImageTitle' : 'title7'
        },
        {
            'id':8,
            'categoryName': 'household',
            'categoryImage' : household,
            'categoryImageTitle' : 'title8'
        },
    ]
}
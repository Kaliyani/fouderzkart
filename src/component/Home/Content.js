import React from 'react'
import './Content.css'

const content = (props) => {
    return(
        <a href={props.link}>
        <figure className="figure">
            <img src={props.image} className="figure-img img-fluid rounded" alt={props.altTextImage}/>
            <figcaption className="figure-caption">
                <h4>{props.title}</h4>
                <p>{props.subTitle}</p>
                <p> <img src={props.rightArrow} className="arrow-icon" alt={props.altTextArrow}/> </p>
            </figcaption>
        </figure>
    </a>
    )
}

export default content
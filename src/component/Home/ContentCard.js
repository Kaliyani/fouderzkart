import React from 'react'
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const styles = {
    card: {
      maxWidth: 345,
      height: 250
    },
    media: {
      // ⚠️ object-fit is not supported by IE 11.
      objectFit: 'cover',
    },
  };

  const ContentCard = (props) => {
    const { classes, image, altTextImage, title, subTitle, rightArrow, altTextArrow } = props;
    return (
      <Card className={classes.card}>
        <CardActionArea>
          <CardMedia
            component="img"
            alt={altTextImage}
            className={classes.media}
            height="140"
            image={image}
            title="Contemplative Reptile"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {title}
            </Typography>
            <Typography component="p">
              {subTitle}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    );
  }

  export default withStyles(styles)(ContentCard)

import React from 'react'
import Content from './Content'
//import Content from './ContentCard'
import Book from '../../assests/book.jpg'
import RightArrow from '../../assests/right-arrow.png'
import HandLooms from '../../assests/handlooms.jpg'
import Sustainable from '../../assests/sustanabletwo.jpg'
import Organic from '../../assests/organic.jpg'
import Handcrafted from '../../assests/handycrafted.jpg'
import { withStyles, withTheme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
    root: {
      flexGrow: 1,
    },
    gridParent:{
        width:'100%',
        padding: 5,
        margin:0
    }
  });

const HomePageGrid = (props) => {
    const { classes, theme } = props;
    console.log(theme)
    return (
        <div className={classes.root} >
            <Grid className={classes.gridParent} container spacing={24} justify='center' alignItems='center' alignContent='space-around'>
                <Grid item sm={3} xs={11}>
                    <Content 
                        link='/books'
                        image={Book}
                        altTextImage = 'Books to Search'
                        title = 'books'
                        subTitle = 'Natural and organic fabrics. Nan-loxic dyes. '
                        rightArrow = {RightArrow}
                        altTextArrow = 'Right Arrow'/>
                    <br/>
                    <br/>
                    <Content 
                        link='/handloom'
                        image={HandLooms}
                        altTextImage = 'HandloomImage'
                        title = 'handloom'
                        subTitle = 'Natural and organic fabrics. Nan-loxic dyes. '
                        rightArrow = {RightArrow}
                        altTextArrow = 'Right Arrow'/>
                </Grid>
                <Grid item sm={3} xs={11} >
                    <Content 
                        link='/socialMedia'
                        image={Sustainable}
                        altTextImage = 'SocialMediaImage'
                        title = 'sustainable'
                        subTitle = 'Natural and organic fabrics. Nan-loxic dyes. Biodegradable materials. '
                        rightArrow = {RightArrow}
                        altTextArrow = 'Right Arrow'/>
                </Grid>
                <Grid item sm={3} xs={11}>
                    <Content 
                        link='/organic'
                        image={Organic}
                        altTextImage = 'OrganicImage'
                        title = 'organic'
                        subTitle = 'Natural and organic fabrics. Nan-loxic dyes. '
                        rightArrow = {RightArrow}
                        altTextArrow = 'Right Arrow'/>
                    <br/>
                    <br/>
                    <Content 
                        link='/handCrafted'
                        image={Handcrafted}
                        altTextImage = 'HandcraftedImage'
                        title = 'handcrafted'
                        subTitle = 'Natural and organic fabrics. Nan-loxic dyes. '
                        rightArrow = {RightArrow}
                        altTextArrow = 'Right Arrow'/>
                </Grid>
            </Grid>
        </div>
        
    )
}

export default withStyles(styles)(withTheme()(HomePageGrid))
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import React, { Component } from 'react';
import Drawer from '@material-ui/core/Drawer';
import { withStyles } from '@material-ui/core/styles';
import {Link} from 'react-router-dom'
import {withRouter} from 'react-router'

const styles = {
    list: {
      width: 250,
    }
};

class SideDrawer extends Component{
    
    
    toggleDrawer = (open) => () => {
        this.props.setValue(false);
    };
    render(){
        const { classes, isOpen, location, history } = this.props;
        console.log('Location object : ' +location)
        console.log('History object : ' +history)

    const sideList = (
      <div className={classes.list}>
        <List>
          {['stores', 'ethical qualities', 'new arrivals', 'event'].map((text, index) => (
            <ListItem button component={Link} to={'/'+text.replace(' ','') } key={text}>
              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
        <Divider />
        <List>
          {['open a shop', 'events', 'search'].map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
      </div>
    );
        return(
            <Drawer open={isOpen} onClose={this.toggleDrawer(false)}>
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer(false)}
            onKeyDown={this.toggleDrawer(false)}
          >
            {sideList}
          </div>
        </Drawer>
        )
    }
}

export default withRouter(withStyles(styles)(SideDrawer))
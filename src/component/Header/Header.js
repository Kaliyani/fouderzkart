import React from 'react'
import NavBar from './Navigation'
import AppHeader from './AppHeader'

const Header = (props) => {
    return (
        <div>
            <AppHeader/>
            <NavBar/>
        </div>
    )
}

export default Header
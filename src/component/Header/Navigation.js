import React from 'react'
import Toolbar from '@material-ui/core/Toolbar';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'

const styles = (theme) => ({
    list: {
      minHeight: 32,
      backgroundColor: '#2196F3',
      [theme.breakpoints.down('xs')]:{
          display : 'none'
      }
    },
    nav: {
        padding: 0,
        margin: 0,
        listStyle: 'none',
        '& li':{
            display: 'inline-block',
            marginLeft: 40
        },
        '& a': {
            color: '#FFF',
            textDecoration: 'none'
        },
        '& :hover': {
            color: '#DDD'
        }
    }
});

function Navigation(props) {
    const {classes} = props
  return (
    <Toolbar className={classes.list}>
        <ul className={classes.nav}>
            <li><Link to='/stores'>stores</Link></li>
            <li><Link to='/ethicalqualities'>ethical qualities</Link></li>
            <li><Link to='/newarrivals'>new arrivals</Link></li>
            <li><Link to='/event'>event</Link></li>
        </ul>
    </Toolbar>
  )
}

export default withStyles(styles)(Navigation)

import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import SearchField from './SearchField'
import Grid from '@material-ui/core/Grid'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faLock } from '@fortawesome/free-solid-svg-icons'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Lock from '@material-ui/icons/Lock'
import Drawer from './SideDrawer'

const styles = (theme) => ({
    root: {
      flexGrow: 1,
    },
    loginIcons: {
      // padding: '11px 15px',
      /* width: 30px; */
      /* height: 30px; */
      border: '2px #a5a5a5 dotted',
      borderRadius: '50%',
      marginLeft: '6px',
      color: '#000000',
      [theme.breakpoints.down('xs')]:{
        display : 'none'
      }
  },
  lockIcon:{
    fontSize: 20
  },
  menuButton: {
    position: 'static',
    float: 'left',
    [theme.breakpoints.up('sm')]:{
      display : 'none'
    }
  },
  title:{
    [theme.breakpoints.down('xs')]:{
      float : 'left',
      padding: 10
    }
  },
  toolbar:{
    minHeight:54
  }
});

class AppHeader extends Component {
  state = {
    isOpen : false,
  };

  setMenuClick = (isClicked) => {
    this.setState({isOpen : isClicked})
  }

  handleDrawerOpen = () =>{
    this.setState({isOpen : true})
  }
  render() {
    
      const {classes} = this.props
    return (
        <div className={classes.root}>
        <AppBar position="static" color="default">
          <Toolbar className={classes.toolbar}>
            <Grid container justify='center' alignItems='center'>
              
              <Grid xs={12} sm={4} md={7} lg={8} item align='left'>
                <IconButton
                    color="inherit"
                    aria-label="Open drawer"
                    onClick={this.handleDrawerOpen}
                    className={classes.menuButton}
                  >
                    <MenuIcon />
                </IconButton>
                <Typography className={classes.title} variant="h6" color="inherit">
                  founderskart
                </Typography>
              </Grid>
              <Grid xs={12} sm={5} md={3} lg={3}  item align='right'>
                <SearchField/>
              </Grid>
              <Grid sm={3} md={2} lg={1} align='right' item>
                  {/* <span className={classes.loginIcons}><FontAwesomeIcon  icon={faLock} aria-hidden="true"></FontAwesomeIcon></span> */}
                  <IconButton className={classes.loginIcons}><Lock className={classes.lockIcon}/></IconButton>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
        <Drawer isOpen={this.state.isOpen} setValue={this.setMenuClick}/>
      </div>
    )
  }
}

export default withStyles(styles)(AppHeader)

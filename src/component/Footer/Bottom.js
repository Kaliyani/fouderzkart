import React from 'react'
import Toolbar from '@material-ui/core/Toolbar';
import { withStyles } from '@material-ui/core/styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const styles = (theme) => ({
    list: {
      minHeight: 32,
      backgroundColor: '#2196F3',
      [theme.breakpoints.down('xs')]:{
          minHeight : 'auto'
      }
    },
    nav: {
        padding: 0,
        margin: 0,
        listStyle: 'none',
        '& li':{
            display: 'inline-block',
            marginLeft: 40
        },
        '& a': {
            color: '#FFF',
            textDecoration: 'none'
        },
        '& :hover': {
            color: '#DDD'
        }
    }
});

function Bottom(props) {
    const {classes} = props
  return (
    <Toolbar className={classes.list}>
        <ul className={classes.nav}>
            <li><a href="#">founderskart</a></li>
            <li><a href="#"><FontAwesomeIcon icon={['fab', 'facebook']}/></a></li>
            <li><a href="#"><FontAwesomeIcon icon={['fab', 'twitter']}/></a></li>
            <li><a href="#"><FontAwesomeIcon icon={['fab', 'google']}/></a></li>
            <li><a href="#">seller login</a></li>
            <li><a href="#">terms of services</a></li>
            <li><a href="#">privacy policy</a></li>
        </ul>
    </Toolbar>
  )
}

export default withStyles(styles)(Bottom)

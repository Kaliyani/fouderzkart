import React from 'react'
import {withStyles} from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'

const styles = {
    card:{
        maxWidth: '100%',
        maxHeight: 150
    },
    media:{
        height: 100
    },
    content:{
        paddingLeft:0,
        paddingRight:0,
        paddingTop:0,
        paddingBottom:0,
        marginBottom:'0.5rem'
    },
    hr:{
        marginTop: '0rem',
        marginBottom: '0.5rem',
    }
}

function ProductCategory(props) {
    const {classes, categoryImage, categoryImageTitle, categoryName} = props
  return (
    <Card className={classes.card}>
        <CardActionArea>
            <CardMedia 
                className={classes.media}
                image={categoryImage}
                title={categoryImageTitle}
            />
            <hr className={classes.hr}/>
            <CardContent className={classes.content}>
                <Typography component='h5'>
                    {categoryName}
                </Typography>
            </CardContent>
        </CardActionArea>
    </Card>
  )
}

export default withStyles(styles)(ProductCategory)

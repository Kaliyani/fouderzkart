export default {
    root: {
      background: 'linear-gradient(45deg, #50af5e 30%, #35b765 90%)',
      borderRadius: 3,
      border: 0,
      color: 'white',
      height: 48,
      padding: '0 30px',
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    },
    label: {
      textTransform: 'lowercase',
    },
    '& hover':{
        background : '#50af5e'
    }
}
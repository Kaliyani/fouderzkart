import React from 'react'
import Button from '@material-ui/core/Button'
import {withStyles} from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Category from './Category'
import category from '../../assests/category'

const styles = {
    button:{
      textTransform : 'none',
      width: '100%'
    },
    parentGrid:{
      paddingTop: 5
    }
}
function CategoryList(props) {
  const {classes} = props
  return (
    <div>
      <Button className={classes.button} variant='outlined' size='small' color='primary'>
        Show All
      </Button>
      <Grid className={classes.parentGrid} container spacing={8}>
        {
          category.categories.map( (category, index) =>(
              <Grid key={index} item sm={12} md={6}>
                <Category 
                    categoryName={category.categoryName}
                    categoryImage={category.categoryImage}
                    categoryImageTitel={category.categoryImageTitle}
                />
              </Grid>
          ))
        }
      </Grid>
      
    </div>
  )
}

export default withStyles(styles)(CategoryList)

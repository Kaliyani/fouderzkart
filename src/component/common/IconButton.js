import React from 'react'
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import styles from './IconButtonStyle'


function IconButton(props) {
    const {classes} = props
  return (
    <div>
      <Button variant="contained" color="secondary" className={classes.button}>
        Delete
        <DeleteIcon className={classes.rightIcon} />
      </Button>
      
    </div>
  )
}

export default withStyles(styles)(IconButton)

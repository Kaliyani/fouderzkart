import React from 'react'
import Card from '@material-ui/core/Card'
import {withStyles} from '@material-ui/core/styles'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import CardActionArea from '@material-ui/core/CardActionArea'
import {withRouter} from 'react-router'

const styles = (theme) => ({
    cardParent : {
        width:'100%',
        height:'100%',
        display: 'flex',
        [theme.breakpoints.down('xs')]:{
            height:'100%'
        }
    },
    cardMedia:{
        width:350,
        [theme.breakpoints.down('xs')]:{
            height:200
        }
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    productCard:{
        width:100,
        height: 100,
        backgroundColor: '#ffe6e6'
    }
})

function FoundersCard(props) {
    const {classes, founderData} = props
    console.log(founderData)
    const founderSelected = () =>{
        props.history.push('/stores/details/2')
    }
    
  return (
    <div>
      <Card className={classes.cardParent}>
        <CardMedia className={classes.cardMedia}
            image={founderData.founderImage}
            title={founderData.foundersImageTitle}
        />
        <div className={classes.details}>
        
        <CardContent className={classes.content}>
        <CardActionArea onClick={founderSelected}>
        <Typography component="h5" variant="h5" align='left'>
                    {founderData.founderName}
                </Typography> 
        </CardActionArea>
                
                <Typography variant="subtitle1" color="textSecondary" align='left'>
                    {founderData.founderDescription}
                </Typography>
                <Typography variant="subtitle1" color="textSecondary" align='left'>
                    {`Products by this founder`}
                </Typography>
                <Grid container spacing={8} sm={12} >
                    {   
                        founderData.totalProduct.map( (product, index) => (
                            <Grid item xs={6} sm={2}>
                                <Card key={index} className={classes.productCard}>
                                
                                </Card>
                            </Grid>
                            
                        ) )
                    }
                </Grid>
            </CardContent>
        
            
            
            
            
            
            
        </div>
      </Card>
    </div>
  )
}

export default withRouter(withStyles(styles)(FoundersCard))

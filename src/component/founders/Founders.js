import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid'
import {withStyles} from '@material-ui/core/styles'
import CategoryList from '../common/CategoryList'
import FoundersList from './FoundersList'
import FounderDetails from './FounderDetails'
import {Route, Switch} from 'react-router-dom'

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  parent:{
    padding: theme.spacing.unit * 1
  },
  category: {
    display : 'none',
    [theme.breakpoints.up('sm')]: {
      display : 'block'
    }
  },
});

class Founders extends Component {
  render() {
    const {classes} = this.props
    return (
      <div>
        <Grid className={classes.parent} container spacing={8} alignItems='stretch'>
          <Grid item sm={2} className={classes.category}>
              <CategoryList/>
          </Grid>
          <Grid item xs ={12}sm={10}>
            <Switch>
              <Route exact path='/stores' component={FoundersList}/>
              <Route path='/stores/details/:id' component={FounderDetails}/>
            </Switch>
          </Grid>
        </Grid>
      </div>
    )
  }
}

export default withStyles(styles)(Founders)

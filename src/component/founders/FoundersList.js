import React, { Component } from 'react'
import FoundersCard from './FoundersCard'
import founders from '../../assests/founders/founders'
import Grid from '@material-ui/core/Grid'

class FoundersList extends Component {
  render() {
    return (
      <Grid container spacing={8} >
        {
          founders.founders.map( (founder) => (
            <Grid item xs={12} sm={12}>
              <FoundersCard key={founder.id} founderData={founder}/>
            </Grid>
          ))
        }
        
      </Grid>
    )
  }
}

export default FoundersList

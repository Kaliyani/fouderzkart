import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid'
import {withStyles} from '@material-ui/core/styles'
import CategoryList from '../common/CategoryList'
import ProductSearchList from './ProductSearchList'
import {Route} from 'react-router-dom'

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  category: {
    display : 'none',
    [theme.breakpoints.up('sm')]: {
      display : 'block'
    }
  },
  parent:{
    padding: theme.spacing.unit * 1
  }
});

class ProductSearch extends Component {
  render() {
    const {classes} = this.props
    return (
      <div>
        <Grid className={classes.parent} container spacing={8} alignItems='stretch'>
          <Grid item sm={2} className={classes.category}>
              <CategoryList/>
          </Grid>
          <Grid item xs ={12}sm={10}>
            <Route path='/product' component={ProductSearchList}/>
          </Grid>
        </Grid>
      </div>
    )
  }
}

export default withStyles(styles)(ProductSearch)

import React from 'react'
import {withStyles} from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import StarRating from 'react-star-rating-component'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'

const styles = (theme) => ({
    card:{
        maxWidth: 350,
        [theme.breakpoints.down('xs')]:{
            maxWidth: '100%'
        }

    },
    media:{
        height: 140
    },
    button:{
        textTransform : 'none'
    }
})

function ProductCard(props) {
    const {classes, productImage, productImageTitle, productName, productDescription, ratingName, rating, price} = props
  return (
    <Card className={classes.card}>
        <CardActionArea>
            <CardMedia 
                className={classes.media}
                image={productImage}
                title={productImageTitle}
            />
            <CardContent>
                <Typography gutterBottom variant='h5' component='h2'>
                    {productName}
                </Typography>
                <Typography component='p' align='left'>
                    {productDescription}
                </Typography>
                <div align='left'>
                    <StarRating >
                        name={ratingName}
                        editing={false}
                        starCount={5}
                        value={rating}
                    </StarRating>
                </div>
                <div align='left'>
                    {`Price : ₹${parseFloat(price).toFixed(2)}`}
                </div>
            </CardContent>
        </CardActionArea>
        <CardActions>
            <Button className={classes.button} variant='outlined'size="small" color="primary">
                Add to cart
            </Button>
            <Button className={classes.button} variant='outlined' size="small" color="primary">
                Buy
            </Button>
        </CardActions>
        
    </Card>
  )
}

export default withStyles(styles)(ProductCard)

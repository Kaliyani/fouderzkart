import React, { Component } from 'react'
import products from '../../assests/data'
import ProductCard from './ProductCard'
import Grid from '@material-ui/core/Grid'

class ProductSearchList extends Component {
  render() {
    return (
      <div>
          <Grid container spacing={8}>
        {
            products.products.map( (product, index) => (
                <Grid item xs={12} sm={6} md={4} lg={4} xl={3}>
                    <ProductCard key={index}
                        productImage={product.productImage}
                        productTitle={product.productImageTitle}
                        productName={product.productName} 
                        productDescription={product.productDescription}
                        ratingName={product.id} 
                        rating={product.productRating}
                        price={product.price}
                    />
                    </Grid>
            ))
            }
            </Grid>
      </div>
    )
  }
}

export default ProductSearchList
